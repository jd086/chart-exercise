import React, { Component } from 'react';
import './App.css';
import Chart from './chart.js';


class App extends Component<> {
    render() {
        return (
            <div className="App" width="1000" height="300">
                <Chart />
            </div>
        );
    }
}

export default App;